PKGS_TO_INSTALL="adduser postgresql python3 python3-dateutil python3-decorator python3-docutils python3-feedparser python3-pil python3-jinja2 python3-ldap3 python3-lxml python3-mako python3-mock python3-openid python3-psutil python3-psycopg2 python3-babel python3-pydot python3-pyparsing python3-pypdf2 python3-reportlab python3-requests python3-simplejson python3-tz python3-vatnumber python3-werkzeug python3-yaml python3-serial python3-pip python3-dev localepurge isc-dhcp-server git rsync openbox python3-netifaces python3-passlib python3-libsass python3-qrcode python3-html2text python3-unittest2 python3-simplejson"

apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install ${PKGS_TO_INSTALL}

adduser --disabled-login pi

cd /home/pi
git clone https://gitlab.com/sofcar/ubuntu_posbox_base.git odoo

pip install pyserial pyusb==1.0.0b1 qrcode evdev babel pypdf

groupadd usbusers
usermod -a -G usbusers pi
usermod -a -G lp pi
usermod -a -G lpadmin pi 

sudo -u postgres createuser -s pi
mkdir /var/log/odoo
chown pi:pi /var/log/odoo

echo 'SUBSYSTEM=="usb", GROUP="usbusers", MODE="0660"
SUBSYSTEMS=="usb", GROUP="usbusers", MODE="0660"' > /etc/udev/rules.d/99-usbusers.rules


echo '[Unit]
Description=Odoo PosBoxLess
After=network.target
[Service]
Type=simple
User=pi
Group=pi
ExecStart=/home/pi/odoo/odoo-bin --load=web,hw_proxy,hw_posbox_homepage,hw_scale,hw_scanner,hw_escpos
KillMode=mixed
[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/posboxless.service

systemctl enable posboxless.service

reboot
